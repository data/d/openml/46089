# OpenML dataset: None

https://www.openml.org/d/46089

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The liver_cirrhosis.csv dataset is a medical dataset focused on patients diagnosed with Liver Cirrhosis. It consists of 20 attributes, capturing a variety of clinical and demographic details. The dataset includes both categorical and continuous variables, such as patient status, medication type, biochemical measurements, physical examination findings, and demographic information. 

Attribute Description:
- N_Days: Number of days between registration and the last follow-up or death (e.g., 1769, 2400).
- Status: Patient's status at last follow-up (D: Died, CL: Censored with Liver failure, C: Censored without Liver failure).
- Drug: Type of medication used (D-penicillamine or Placebo).
- Age: Age of the patient in days at the start of the study (e.g., 25514).
- Sex: Gender of the patient (F: Female, M: Male).
- Ascites: Presence of Ascites (Y: Yes, N: No).
- Hepatomegaly: Enlargement of the liver (Y: Yes, N: No).
- Spiders: Presence of spider naevi, a type of angioma (Y: Yes, N: No).
- Edema: Presence and severity of Edema (N: No, S: Slight).
- Bilirubin: Serum Bilirubin in mg/dl (e.g., 5.0).
- Cholesterol: Serum Cholesterol in mg/dl (e.g., 200.0).
- Albumin: Serum Albumin in g/dl (e.g., 3.35).
- Copper: Serum Copper in ug/dl (e.g., 82.0).
- Alk_Phos: Alkaline Phosphatase in U/l (e.g., 1982.655769).
- SGOT: Serum Glutamic-Oxaloacetic Transaminase in U/ml (e.g., 57.35).
- Triglycerides: Serum Triglycerides in mg/dl (e.g., 58.0).
- Platelets: Platelet count per cubic millimeter (e.g., 225.0).
- Prothrombin: Prothrombin time as a percentage of normal (e.g., 9.9).
- Stage: Clinical stage of Cirrhosis (1-4).

Use Case:
This dataset is essential for researchers and healthcare professionals studying the progression and treatment outcomes of Liver Cirrhosis. It can be used for predictive modeling to identify key indicators of prognosis, treatment efficiency analysis, and understanding demographic correlations with disease outcomes. Moreover, it serves as a valuable resource for training machine learning models aimed at predicting patient survival, response to treatment, and disease progression, contributing to personalized medicine development and better clinical management of Liver Cirrhosis.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46089) of an [OpenML dataset](https://www.openml.org/d/46089). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46089/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46089/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46089/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

